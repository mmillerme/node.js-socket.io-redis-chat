An experiment where I began to learn Node.js, Socket.IO, and Redis by developing a live chat system.

You can private-message other users if the message is formatted like so: "/Username Message to user".