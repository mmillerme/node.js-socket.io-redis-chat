(function ($) {
	$.fn.highlight = function () {
		var to_color = $(this).is('.user.you') ? 'rgba(227, 248, 250, 1.00)' : ($(this).is(':nth-child(even)') ? 'rgba(238, 238, 238, 1.00)' : 'rgba(255, 255, 0, 0.00)');
		return this
			.css('background-color', 'rgba(255, 255, 0, 0.75)')
			.animate({'background-color': $.Color(to_color)});
	};

	$(window).resize(function () {
		$('#online, #messages').outerHeight(true, $(window).height()-($('body').outerHeight(true)-$('body').height())-Math.max($('#join').outerHeight(true), $('#send').outerHeight(true)));
		$('#messages').prop('scrollTop', $('#messages').prop('scrollHeight'));
	}).resize();

	$('#your-name').focus();

	$('#message')
		.keypress(function (v) {
			if (v.keyCode == 13 && !v.shiftKey && $.trim($(this).val())) {
				$('#send').submit();
			}
		})
		.keyup(function (v) {
			if (v.keyCode == 13 && !v.shiftKey) {
				$(this).val('');
			}
		});

	var socket = io.connect('/', {'sync disconnect on unload': true});

	var add_message = function (classes, message) {
		var $li = $('<li></li>')
			.addClass(classes)
			.append(message)
			.html(function (i, html) {
				return html.replace(/\n/g, '<br>\n');
			});

		$('#messages').append($li);

		if (!$li.is('.preload')) {
			$li.highlight();
		}

		$(window).resize();
	};

	var add_user_message = function (name, message, timestamp, classes, recipients) {
		add_message(
			'user'+($.trim(name).toLowerCase() == $.trim($('#your-name').val()).toLowerCase() ? ' you' : '')+(classes ? ' '+classes : ''),
			$('<span></span>')
				.attr('title', timestamp)
				.addClass('timestamp')
				.prop('outerHTML')+
			$('<span></span>')
				.text(name)
				.addClass('name')
				.prop('outerHTML')+(
			recipients ?
				$('<span></span>')
					.addClass('recipients')
					.text(recipients.join(', '))
					.prop('outerHTML')
				:
					''
			)+
			$('<span></span>')
				.html(message)
				.prop('outerHTML')
		)
		$('#messages li:last .timestamp').timeago();
	};

	socket.on('connect', function () {
		$('#join').submit(function (v, is_cookie) {
			$('#your-name').val($.trim($('#your-name').val()));
			if (!/^[A-Za-z0-9\.\-_]+$/.test($('#your-name').val())) {
				alert('Please use only letters, digits, ., -, and _ in your name.');
				return;
			}
			socket.emit('chat:join', $('#your-name').val(), function (error) {
				if (error) {
					alert(!is_cookie ? 'The name you entered is already in use.' : 'It appears someone else has already logged in using your name.\nPlease enter a different name.');
					$('#your-name').select();
				} else {
					$('#join').hide();
					$('#send').show();
					$('#message').focus();
					$.cookie('chat_name', $('#your-name').val());
				}
			});
		});

		if ($.cookie('chat_name')) {
			$('#your-name').val($.cookie('chat_name'));
			$('#join').trigger('submit', true);
		}

		socket.on('chat:connect', function (messages, users) {
			$('#messages').empty();
			$.each(messages.reverse(), function (i, message) {
				add_user_message(message.name, message.message, message.timestamp, 'preload'+(message.pm ? ' pm' : ''), message.recipients ? message.recipients : false);
			});

			$.each(users, function (name, status) {
				if (name == $('#your-name').val()) {
					return true;
				}
				var $li = $('<li></li>').text(name);
				$li.addClass(status);
				$('#online-users').append($li);
			});
		});

		socket.on('chat:join', function (name) {
			add_message(
				'join',
				$('<span></span>')
					.addClass('name')
					.text(name)
					.prop('outerHTML')+
				' joined the session.'
			);

			$('#online-users').append($('<li></li>').text(name).highlight());
			$('#online').prop('scrollTop', $('#online').prop('scrollHeight'));
		});

		socket.on('chat:leave', function (name) {
			add_message(
				'leave',
				$('<span></span>')
					.addClass('name')
					.text(name)
					.prop('outerHTML')+
					' left the session.'
			)

			$('#online-users li')
				.filter(function () { return $(this).text() == name; })
				.css('background-color', '#FFAAAA')
				.fadeOut(400, function () {
					$(this).remove();
				});
		});

		socket.on('chat:status', function (name, status) {
			$('#online-users li').filter(function () { return $(this).text() == name; }).removeClass('active idle typing').addClass(status);
		})

		socket.on('chat:message', function (message) {
			add_user_message(message.name, message.message, message.timestamp);
		});

		socket.on('chat:pm', function (message) {
			add_user_message(message.name, message.message, message.timestamp, 'pm', message.recipients ? message.recipients : false);
		})

		$('#send').submit(function () {
			var message = $('#message').val();
			if (message.indexOf('/') == 0) {
				var split = message.substr(1).split(' ');
				if (split.length < 2) {
					alert('To send a private message, use this format: /User1,User2 My message here.');
					return;
				} else {
					var recipients = split.shift();
					message = split.join(' ');
					socket.emit('chat:pm', recipients.split(','), message, function () {
						alert('You must specify at least one recipient who is currently online.');
					});
				}
			} else {
				socket.emit('chat:message', $('#message').val());
				$('#message').focus();
			}
		});

		var typing_timeout;
		var sent_typing = false;
		$('#message').keypress(function (v) {
			if (v.which == 13 && !v.shiftKey) {
				return;
			}

			if (!sent_typing) {
				socket.emit('chat:status', 'typing');
				sent_typing = true;
			}
			if (typing_timeout) {
				clearTimeout(typing_timeout);
			}
			typing_timeout = setTimeout(function () {
				socket.emit('chat:status', 'active');
				sent_typing = false;
			}, 500);
		});

		var status_timeout;
		var sent_status = false;
		$('body').on('mousemove keydown', function () {
			if (!sent_status) {
				socket.emit('chat:status', 'active');
				sent_status = true;
			}
			if (status_timeout) {
				clearTimeout(status_timeout);
			}
			status_timeout = setTimeout(function () {
				socket.emit('chat:status', 'idle');
				sent_status = false;
			}, 60000);
		})
	});
})(jQuery);