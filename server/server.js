var http = require('http');
var fs = require('fs');
var path = require('path');
var url = require('url');

var MIME_TYPES = {
	'html': 'text/html',
	'jpeg': 'image/jpeg',
	'jpg': 'image/jpg',
	'png': 'image/png',
	'js': 'text/javascript',
	'css': 'text/css',
	'json': 'application/json'
};

var server = http.createServer(function (request, response) {
	var uri = url.parse(request.url).pathname;
	if (uri.charAt(uri.length-1) == '/') {
		uri += 'index.html';
	}

	if (uri.indexOf('..') != -1) {
		response.writeHead(403);
		response.end();
	}

	var filename = __dirname+'/../www/'+uri;

	fs.exists(filename, function (exists) {
		if (!exists) {
			response.writeHead(404);
			response.end('404');
			return;
		}

		response.writeHead(200, {'Content-Type': MIME_TYPES[path.extname(filename).split('.')[1]]});

		fs.createReadStream(filename).pipe(response);
	});
}).listen(90);

require('./chat/chat').run(server);