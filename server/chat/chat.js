exports.run = function (server) {
	var io = require('socket.io').listen(server, {log: false});
	io.configure('production', function () {
		io.set('log level', 1);
		io.enable('browser client minification');
		io.enable('browser client etag');
		io.set('transports', ['websocket', 'flashsocket', 'htmlfile', 'xhr-polling', 'jsonp-polling']);
		io.set('origins', '*:*');
	});
	var redis = require('redis').createClient();

	var online_users = {};
	var sockets = {};
	var messages = [];

	var is_user = function (name) {
		var user_names = [];
		var lower_user_names = [];
		for (var _name in online_users) {
			user_names.push(_name);
			lower_user_names.push(_name.toLowerCase());
		}
		var index = lower_user_names.indexOf(name.toLowerCase());
		return index > -1 ? user_names[index] : false;
	};

	redis.lrange('chat:messages', 0, -1, function (err, _messages) {
		messages = _messages.map(JSON.parse);
		io.on('connection', function (client) {
			client.on('chat:join', function (name, callback) {
				if (is_user(name) !== false) {
					callback(true);
				} else {
					online_users[name] = 'active';
					sockets[name] = client.id;
					client.set('chat:name', name);
					var _messages = [];
					for (var i = 0; i < messages.length; ++i) {
						var message = messages[i];
						if (!message.recipients) {
							_messages.push(message);
						} else if (is_user(message.name) == name) {
							message.pm = true;
							_messages.push(message);
						} else {
							message.recipients.forEach(function (recipient) {
								recipient = is_user(recipient);
								if (recipient !== false && recipient == name) {
									var copy = JSON.parse(JSON.stringify(message));
									delete copy.recipients;
									copy.pm = true;
									_messages.push(copy);
								}
							});
						}
					}
					io.sockets.emit('chat:join', name);
					client.emit('chat:connect', _messages, online_users);
					callback(false);
				}
			});

			client.on('chat:message', function (message) {
				client.get('chat:name', function (err, name) {
					message = {'name': name, 'message': message, 'timestamp': new Date()};
					io.sockets.emit('chat:message', message);
					messages.unshift(message);
					redis.lpush('chat:messages', JSON.stringify(message), function (err, reply) {
						redis.ltrim('chat:messages', 0, 1000);
					});
				});
			});

			client.on('chat:pm', function (recipients, message, error_callback) {
				client.get('chat:name', function (err, name) {
					message = {'name': name, 'message': message, 'timestamp': new Date()};
					for (var i = recipients.length-1; i >= 0; --i) {
						var recipient = recipients[i] = is_user(recipients[i]);
						if (recipient !== false && (recipient in sockets)) {
							io.sockets.socket(sockets[recipient]).emit('chat:pm', message);
						} else {
							recipients.splice(i, 1);
						}
					}
					message.recipients = recipients;
					if (!message.recipients.length) {
						error_callback();
					} else {
						client.emit('chat:pm', message);
						messages.unshift(message);
						redis.lpush('chat:messages', JSON.stringify(message), function (err, reply) {
							redis.ltrim('chat:messages', 0, 1000);
						});
					}
				});
			});

			client.on('chat:status', function (status) {
				client.get('chat:name', function (err, name) {
					if (name) {
						io.sockets.emit('chat:status', name, status);
						online_users[name] = status;
					}
				});
			});

			client.on('disconnect', function () {
				client.get('chat:name', function (err, name) {
					if (name) {
						io.sockets.emit('chat:leave', name);
						if (name in online_users) {
							delete online_users[name];
						}
					}
				});
			});
		});
	});
};